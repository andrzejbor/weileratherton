﻿#include "DefsCV.h"
#include "geom.h"
#include "WeilerAtherton.h"
#include "BentleyOttmann.h"
#include "IntersectionPoint.h"
#include "Debug.h"

struct PointLink
{
	int kind; //0 - punkt z wielokąta 0, 1 - z wielokąta 1, 2: punkt przecięcia
	int index; //do debugowania
	double x; //współrzędne
	double y;
	struct PointLink *origlink; //nie będzie zmieniany
	struct PointLink *backlink; //nie będzie zmieniany
	struct PointLink *link0; //tylko punkty przecięcia będą miały obydwa linki
	struct PointLink *link1;
	int visitCnt;
};

typedef struct PointLink* PPointLink;

PPointLink list0, list1, pplist;

double getRelativePosition(double cx, double cy, double x0, double y0, double x1, double y1)
{
	if (fabs(x1 - x0) > fabs(y1 - y0))
	{//bardziej pozioma
		return (cx - x0) / (x1 - x0);
	}
	else
	{//bardziej pionowa
		return (cy - y0) / (y1 - y0);
	}
}

vector<Poly*> polygonsWork;

void brute_force_intersections()
{
	//szukamy przecięcia krawędzi jednego wielokąta z drugim
	PPointLink prevpp = NULL;
	int index = 0;
	Poly *polygonA = polygonsWork[0];
	Poly *polygonB = polygonsWork[1];
	vector<IntersectionPoint> intersectPoints;

	PPointLink l0 = list0;
	for (int i = 0; i < polygonA->n; i++)
	{
		Line segA;
		Point p;
		p = polygonA->pts[i];
		segA.x0 = p.x;
		segA.y0 = p.y;
		if (i == polygonA->n - 1)
			p = polygonA->pts[0];
		else
			p = polygonA->pts[i + 1];
		segA.x1 = p.x;
		segA.y1 = p.y;
		PPointLink l1 = list1;
		for (int j = 0; j < polygonB->n; j++)
		{
			Line segB;
			Point p1;
			p1 = polygonB->pts[j];
			segB.x0 = p1.x;
			segB.y0 = p1.y;
			if (j == polygonB->n - 1)
				p1 = polygonB->pts[0];
			else
				p1 = polygonB->pts[j + 1];
			segB.x1 = p1.x;
			segB.y1 = p1.y;
			double fx, fy;
			if (segment_segment_intersection(segA, segB, fx, fy))
			if ((fx != segA.x1 || fy != segA.y1) && (fx != segB.x1 || fy != segB.y1)) //aby punkty się nie powtarzały, nie bierzemy końców
			{
				IntersectionPoint intersectPoint;
				intersectPoint.x = fx;
				intersectPoint.y = fy;
				intersectPoint.index[0] = i;
				intersectPoint.index[1] = j;
				intersectPoints.push_back(intersectPoint);
			}
			l1 = l1->origlink;
		}
		l0 = l0->origlink;
	}

	PPointLink *linkTab0, *linkTab1; //tablice wskaźników na PointLink
	vector<BOLine*> lineVector;
	linkTab0 = new PPointLink[polygonA->n];
	l0 = list0;
	for (int i = 0; i < polygonA->n; i++)
	{
		linkTab0[i] = l0;
		l0 = l0->origlink;
		Point p0,p1;
		p0 = polygonA->pts[i];
		if (i == polygonA->n - 1)
			p1 = polygonA->pts[0];
		else
			p1 = polygonA->pts[i + 1];
		BOLine* line0 = new BOLine(p0, p1, NULL, 0, 0);
		lineVector.push_back(line0);
	}
	linkTab1 = new PPointLink[polygonB->n];
	PPointLink l1 = list1;
	for (int i = 0; i < polygonB->n; i++)
	{
		linkTab1[i] = l1;
		l1 = l1->origlink;
		Point p0, p1;
		p0 = polygonB->pts[i];
		if (i == polygonB->n - 1)
			p1 = polygonB->pts[0];
		else
			p1 = polygonB->pts[i + 1];
		BOLine* line1 = new BOLine(p0, p1, NULL, 1, 0);
		lineVector.push_back(line1);
	}

	BentleyOttmann bo;
	bo.addToEventQueue(lineVector);
	bo.run();

	for (int i = 0; i < lineVector.size(); i++)
		delete lineVector[i];

	for (int i = 0; i < intersectPoints.size(); i++)
	{
		IntersectionPoint intersectPoint = intersectPoints[i];
		PPointLink l0 = linkTab0[intersectPoint.index[0]];
		PPointLink l1 = linkTab1[intersectPoint.index[1]];
		//printf("a[%d] z b[%d]: w %f,%f\n", i, j, fx, fy);
		struct PointLink* pp = new PointLink; //punkt przecięcia
		pp->kind = 2;
		pp->index = index;
		pp->x = intersectPoint.x;
		pp->y = intersectPoint.y;
		pp->origlink = NULL;
		pp->backlink = NULL;
		pp->visitCnt = 0;

		PPointLink piter, prevpiter;
		double thisRel;
		//
		thisRel = getRelativePosition(intersectPoint.x, intersectPoint.y, l0->x, l0->y, l0->origlink->x, l0->origlink->y);
		piter = l0->link0;
		prevpiter = l0;
		bool inserted = false;
		while (piter->kind == 2)
		{
			double otherRel = getRelativePosition(piter->x, piter->y, l0->x, l0->y, l0->origlink->x, l0->origlink->y);
			if (thisRel <= otherRel && !inserted)
			{
				pp->link0 = piter;
				prevpiter->link0 = pp;
				inserted = true;
			}
			prevpiter = piter;
			piter = piter->link0;
		}
		if (!inserted)
		{
			pp->link0 = piter;
			prevpiter->link0 = pp;
		}
		//To samo względem punktów wielokąta 1
		thisRel = getRelativePosition(intersectPoint.x, intersectPoint.y, l1->x, l1->y, l1->origlink->x, l1->origlink->y);
		piter = l1->link1;
		prevpiter = l1;
		inserted = false;
		while (piter->kind == 2)
		{
			double otherRel = getRelativePosition(piter->x, piter->y, l1->x, l1->y, l1->origlink->x, l1->origlink->y);
			if (thisRel <= otherRel && !inserted)
			{
				pp->link1 = piter;
				prevpiter->link1 = pp;
				inserted = true;
			}
			prevpiter = piter;
			piter = piter->link1;
		}
		if (!inserted)
		{
			pp->link1 = piter;
			prevpiter->link1 = pp;
		}
		///tutaj origlink dla punktów przecięcia
		if (prevpp == NULL) pplist = pp;
		else prevpp->origlink = pp;
		//pp->backlink = prevpp;
		prevpp = pp;
		index++;
	}
	delete linkTab0;
	delete linkTab1;
}

void makeList(Poly *polygon, PointLink *&list, int numPoly)
{
	assert(numPoly == 0 || numPoly == 1);
	struct PointLink* prevpl = NULL;
	struct PointLink* pl;
	for (int i = 0; i < polygon->n; i++)
	{
		Point p = polygon->pts[i];
		pl = new PointLink;
		pl->visitCnt = 0;
		pl->kind = numPoly;
		pl->index = i;
		pl->x = p.x;
		pl->y = p.y;
		pl->link0 = NULL;
		pl->link1 = NULL;
		if (prevpl == NULL)
			list = pl;
		else
		{
			if (numPoly == 0)
				prevpl->link0 = pl;
			else
				prevpl->link1 = pl;
			prevpl->origlink = pl;
			pl->backlink = prevpl;
		}
		prevpl = pl;
	}
	//koniec wskazuje na początek
	if (numPoly == 0)
		pl->link0 = list;
	else
		pl->link1 = list;
	pl->origlink = list;
	list->backlink = pl;
}


void deleteLinkList(PointLink *list)
{
	PointLink *next;
	PointLink *prev = list;
	do//nie != NULL bo cykliczna
	{
		next = prev->origlink;
		delete prev;
		prev = next;
	} while (prev != list);
}

void deletePPLinkList(PointLink *list)
{
	PointLink *next;
	PointLink *prev = list;
	while(prev != NULL)
	{
		next = prev->origlink;
		delete prev;
		prev = next;
	};
}


Mat mat;

void showPolygons()
{
	mat = Mat::zeros(500, 700, CV_8UC3);
	drawPoly(mat, polygonsWork[0], Scalar(245, 0, 255));
	drawPoly(mat, polygonsWork[1], Scalar(245, 128, 0));
}

void unionWalk(Poly *polyOut, long long sum0, long long sum1, bool debugshow)
{
	/*
	Aby otrzymać unię wielokątów należy wybrać zewnętrzny punkt, aby punkt wierzchołka nie leżał wewnątrz
	Można zrobić w ten sposób, że wybiera się punkt o współrzędnej ekstremalnej w jednym
	z czterech kieunków : minx,miny, maxx,maxy
	wybierając zarówno z pierwszego jak i drugiego wielokąta
	*/
	vector<struct PointLink*> pointList;
	int minx0, minx1;
	struct PointLink *minxP0, *minxP1, *startp, *alterstartp, *pp;
	startp = list0;
	pp = startp;
	minxP0 = pp;
	minx0 = minxP0->x;
	do
	{
		if (pp->x < minx0)
		{
			minxP0 = pp;
			minx0 = minxP0->x;
		}
		pp = pp->origlink;
	} while (pp != startp);

	startp = list1;
	pp = startp;
	minxP1 = pp;
	minx1 = minxP1->x;
	do
	{
		if (pp->x < minx1)
		{
			minxP1 = pp;
			minx1 = minxP1->x;
		}
		pp = pp->origlink;
	} while (pp != startp);

	int numPoly;
	if (sum0!=0 && sum1!=0 || sum0==0 && sum1==0)
	{
		if (minx0 <= minx1)
		{
			numPoly = 0;
			startp = minxP0;
			alterstartp = minxP1;
		}
		else
		{
			numPoly = 1;
			startp = minxP1;
			alterstartp = minxP0;
		}
	}
	else if (sum0!=0)
	{
		numPoly = 0;
		startp = minxP0;
		alterstartp = minxP1;
	}
	else if (sum1!=0)
	{
		numPoly = 1;
		startp = minxP1;
		alterstartp = minxP0;
	}
	if (minxP0->x!=minxP1->x || minxP0->y!=minxP1->y) alterstartp = startp;

	struct PointLink *prevp;
	pp = startp;
	prevp = pp;
	//printf("prevp: kind=%d index=%d (%f,%f)\n", pp->kind, pp->index, pp->x, pp->y);
	pointList.push_back(prevp);
	do
	{
		if (numPoly == 0)
			pp = pp->link0;
		else
			pp = pp->link1;
		//printf("pp: kind=%d index=%d (%f,%f)\n", pp->kind, pp->index, pp->x, pp->y);

		if (pp->kind == 2)
		{
			if (prevp->kind != 2 && fabs(pp->x-prevp->x)<1e-6 && fabs(pp->y-prevp->y)<1e-6)
			{
				//printf("dotyka\n");
				struct PointLink *nextp,*prev2p,*pother,*potherstart;
				nextp = prevp->origlink;
				prev2p = prevp->backlink;
				pother = pp;
				do
				{
					if (numPoly == 0)
						pother = pother->link1; //odwrotnie bo jest to linia drugiego wielokąta z którą się przecina
					else
						pother = pother->link0;
				}
				while  (pother->kind==2);
				potherstart = pother->backlink;

				if (potherstart->x==pp->x && potherstart->y==pp->y)
				{
					//printf("na wierzcholku\n");
					double phi0 = atan2(prev2p->y - pp->y, prev2p->x - pp->x);
					double phi1 = atan2(nextp->y - pp->y, nextp->x - pp->x);
					double phi2 = atan2(pother->y - pp->y, pother->x - pp->x);
					double deltaPhi10 = phi1-phi0;
					if (deltaPhi10<0) deltaPhi10 += 2*M_PI;
					if (deltaPhi10>=2*M_PI) deltaPhi10 -= 2*M_PI;
					double deltaPhi20 = phi2-phi0;
					if (deltaPhi20<0) deltaPhi20 += 2*M_PI;
					if (deltaPhi20>=2*M_PI) deltaPhi20 -= 2*M_PI;
					if (deltaPhi10>deltaPhi20) numPoly = 1 - numPoly;
				}
				else
				{
					//sprawdzenie czy leżą po tej samej stronie
					//najpierw sprawdzimy czy nie są współliniowe
					double detPrev,detNext; //wyznaczniki macierzy
					Line seg;
					seg.x0 = potherstart->x;
					seg.y0 = potherstart->y;
					seg.x1 = pother->x;
					seg.y1 = pother->y;
					Point z;
					z.x = prev2p->x;
					z.y = prev2p->y;
					detPrev = det_matrix(seg, z);
					z.x = nextp->x;
					z.y = nextp->y;
					detNext = det_matrix(seg, z);
					if (fabs(detPrev) > 1e-6 && fabs(detNext) > 1e-6)
					{	//nie współliniowe
						if (detPrev>0 && detNext>0 || detPrev<0 && detNext<0)
						{
							//printf("odbija się, nie zmieniamy wielokata\n");
						}
						else
						{
							//printf("po przeciwnych stronach - zmieniamy wielokat\n");
							numPoly = 1 - numPoly;
						}
					}
					else
					{
						if (fabs(detNext) <= 1e-6)
						{
							//printf("przyklejony, nie zmieniamy wielokata\n");
						}
						else
						{
							//obliczamy orientację trójkąta takiego, że idziemy pierwszym wielokątem
							//i wracamy drugim
							long long sum = 0;
							sum += (pp->link0->x - pp->x)*(pp->link0->y + pp->y);
							sum += (pp->link1->x - pp->link0->x)*(pp->link1->y + pp->link0->y);
							sum += (pp->x - pp->link1->x)*(pp->y + pp->link1->y);
							//jeśli suma==0, zmieniamy numPoly
							if(sum<0) numPoly=0;
							else if(sum>0) numPoly=1;
							else if (sum==0) numPoly = 1 - numPoly;
						}
					}
				}
			}
			else
			  numPoly = 1 - numPoly;
			if (pp->visitCnt > 1) break;
			pp->visitCnt++;
		}
		else //punkt wielokąta a nie punkt przecięcia
		{
			if (pp->visitCnt>0) break;
			pp->visitCnt++;
		}
		Point p0 = Point((int)round(prevp->x), (int)round(prevp->y));
		Point p1 = Point((int)round(pp->x), (int)round(pp->y));
		if (debugshow)
		{
			printf("p0:%d,%d, p1:%d,%d\n", p0.y, p0.x, p1.y, p1.x);
			scalePoints(p0, p1);
			line(mat, p0, p1, Scalar(255, 255, 255), 1, 1, 0);
			imshow("mat", mat);
			cvWaitKey(0);
		}
		if (pp == startp || pp == alterstartp) break;
		pointList.push_back(pp);
		prevp = pp;
	} while (pp != startp && pp != alterstartp);
	polyOut->SetSize(pointList.size());
	for (int i = 0; i < pointList.size(); i++)
	{
		struct PointLink *pp = pointList[i];
		Point p;
		p.x = pp->x;
		p.y = pp->y;
		polyOut->pts[i] = p;
	}
}

// na koniec sprawdza czy wszystkie punkty ekstremalne były odwiedzone
bool checkProcess()
{
	int minx0, miny0, maxx0, maxy0,
		minx1, miny1, maxx1, maxy1;
	struct PointLink *minxP0, *minyP0,*maxxP0, *maxyP0,
					 *minxP1, *minyP1,*maxxP1, *maxyP1,
					 *startp, *pp;
	bool isminx,isminy,ismaxx,ismaxy;
	startp = list0;
	pp = startp;
	minxP0 = pp;
	minx0 = minxP0->x;
	do
	{
		if (pp->x < minx0)
		{
			minxP0 = pp;
			minx0 = minxP0->x;
		}
		pp = pp->origlink;
	} while (pp != startp);

	pp = startp;
	minyP0 = pp;
	miny0 = minyP0->y;
	do
	{
		if (pp->y < miny0)
		{
			minyP0 = pp;
			miny0 = minyP0->y;
		}
		pp = pp->origlink;
	} while (pp != startp);

	startp = list0;
	pp = startp;
	maxxP0 = pp;
	maxx0 = maxxP0->x;
	do
	{
		if (pp->x > maxx0)
		{
			maxxP0 = pp;
			maxx0 = maxxP0->x;
		}
		pp = pp->origlink;
	} while (pp != startp);

	pp = startp;
	maxyP0 = pp;
	maxy0 = maxyP0->y;
	do
	{
		if (pp->y > maxy0)
		{
			maxyP0 = pp;
			maxy0 = maxyP0->y;
		}
		pp = pp->origlink;
	} while (pp != startp);

	//drugi wielokąt
	startp = list1;
	pp = startp;
	minxP1 = pp;
	minx1 = minxP1->x;
	do
	{
		if (pp->x < minx1)
		{
			minxP1 = pp;
			minx1 = minxP1->x;
		}
		pp = pp->origlink;
	} while (pp != startp);

	pp = startp;
	minyP1 = pp;
	miny1 = minyP1->y;
	do
	{
		if (pp->y < miny1)
		{
			minyP1 = pp;
			miny1 = minyP1->y;
		}
		pp = pp->origlink;
	} while (pp != startp);

	startp = list1;
	pp = startp;
	maxxP1 = pp;
	maxx1 = maxxP1->x;
	do
	{
		if (pp->x > maxx1)
		{
			maxxP1 = pp;
			maxx1 = maxxP1->x;
		}
		pp = pp->origlink;
	} while (pp != startp);

	pp = startp;
	maxyP1 = pp;
	maxy1 = maxyP1->y;
	do
	{
		if (pp->y > maxy1)
		{
			maxyP1 = pp;
			maxy1 = maxyP1->y;
		}
		pp = pp->origlink;
	} while (pp != startp);

	if (minx0==minx1)
		isminx = minxP0->visitCnt>0 || minxP1->visitCnt>0;
	else if (minx0<minx1)
		isminx = minxP0->visitCnt>0;
	else
		isminx = minxP1->visitCnt>0;

	if (miny0==miny1)
		isminy = minyP0->visitCnt>0 || minyP1->visitCnt>0;
	else if (miny0<miny1)
		isminy = minyP0->visitCnt>0;
	else
		isminy = minyP1->visitCnt>0;

	if (maxx0==maxx1)
		ismaxx = maxxP0->visitCnt>0 || maxxP1->visitCnt>0;
	else if (maxx0>maxx1)
		ismaxx = maxxP0->visitCnt>0;
	else
		ismaxx = maxxP1->visitCnt>0;

	if (maxy0==maxy1)
		ismaxy = maxyP0->visitCnt>0 || maxyP1->visitCnt>0;
	else if (maxy0>maxy1)
		ismaxy = maxyP0->visitCnt>0;
	else
		ismaxy = maxyP1->visitCnt>0;
	return isminx && isminy && ismaxx && ismaxy;
}

void descPolygon(Poly *poly)
{
	int minx = 1000000000, miny = 1000000000, maxx = -1000000000, maxy = -1000000000;
	printf("polygon ma %d wierzcholkow\n", poly->n);
	for (int i = 0; i < poly->n; i++)
	{
		Point p = poly->pts[i];
		printf("polygon->pts[%d] = Point(%d, %d);\n", i, p.x, p.y);
		minx = min(minx, p.x);
		miny = min(miny, p.y);
		maxx = max(maxx, p.x);
		maxy = max(maxy, p.y);
	}
	printf("%d,%d - %d,%d\n",minx,miny,maxx,maxy);
}

bool WeilerAtherton(Poly *polyA, Poly *polyB, Poly *polyOut, bool debugshow)
{
	long long sum0, sum1;
	Poly *poly0 = new Poly();
	toClockwise(polyA, poly0, false, sum0);
	polygonsWork.push_back(poly0);
	Poly *poly1 = new Poly();
	toClockwise(polyB, poly1, false, sum1);
	polygonsWork.push_back(poly1);
	makeList(polygonsWork[0], list0, 0);
	makeList(polygonsWork[1], list1, 1);
	if (sum0!=0 && sum1!=0)
		brute_force_intersections();
	if (debugshow) {
		showPolygons();
		imshow("mat", mat);
		cvWaitKey(0);
	}
	Poly tmppolyOut;
	unionWalk(&tmppolyOut, sum0, sum1, false);
	bool result = checkProcess();
	if (result)
	{
		polyOut->SetSize(tmppolyOut.n);
		for (int k=0; k<tmppolyOut.n; k++)
			polyOut->pts[k] = tmppolyOut.pts[k];
	}
	if (result && debugshow) {
		drawPoly(mat, polyOut, Scalar(255, 255, 255));
		imshow("mat", mat);
		cvWaitKey(0);
	}
	delete poly0;
	delete poly1;
	polygonsWork.clear();
	deleteLinkList(list0);
	list0 = NULL;
	deleteLinkList(list1);
	list1 = NULL;
	deletePPLinkList(pplist);
	pplist = NULL;
	return result;
}
