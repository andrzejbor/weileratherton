#pragma  once
#include <iostream>
#include "BOLine.h"
using namespace std;

struct BOLine;

struct NodeAvl
{
	NodeAvl* parent; // rodzic
	NodeAvl* left; // lewe dziecko
	NodeAvl* right; // prawe dziecko
	BOLine* pValue; // wartosc
	int balanceFactor; // wspolczynnik wywazenia
};

class AVL
{
private:

	NodeAvl* root; // korzen


	// rotacje
	NodeAvl* RR(NodeAvl* node);
	NodeAvl* LL(NodeAvl* node);
	NodeAvl* RL(NodeAvl* node);
	NodeAvl* LR(NodeAvl* node);

	void Insert(NodeAvl* node);



	NodeAvl* maxNode(NodeAvl* x);
	NodeAvl* minNode(NodeAvl* x);


	NodeAvl* Remove(NodeAvl* node);
	// poprzednia wartosc (jesli chodzi o porzadek)


	void Print(ostream& out, NodeAvl* node) const;
	void Clear(NodeAvl *node); //wo�ane tylko z destruktora
public:

	AVL();
	~AVL();

	NodeAvl* Insert(BOLine* pValue);
	bool Find(BOLine* pValue);
	NodeAvl* Find(BOLine* pValue, bool r);

	void Remove(BOLine* pValue);

	friend ostream & operator<<(ostream &ost, const AVL &avl);

	NodeAvl* pred(NodeAvl* x);
	NodeAvl* succ(NodeAvl* x);
};
