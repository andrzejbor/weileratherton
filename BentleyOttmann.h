#pragma once
#include <iostream>
#include <queue>
#include <set>
#include "Compare.h"
#include "AVL.h"
#include "geom.h"
#include "IntersectionPoint.h"

using namespace std;

class BentleyOttmann
{
private:
	priority_queue<Event, vector<Event>, Compare> eventQueue;
	AVL* avl;
	set<BOLine*> linesSet;
	bool debug;
public:
	BentleyOttmann();
	~BentleyOttmann();
	vector<IntersectionPoint> intersectionVector;
	void addToEventQueue(vector<BOLine*> lineVector);
	void run();
};