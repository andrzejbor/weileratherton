#pragma  once
#include <cmath>

static double EPS = 0.0001;

extern bool EQ(double x, double y);

enum class BOEventType
{
	START,
	END,
	INTERSECTION
};