#include "BentleyOttmann.h"
#include "IntersectionPoint.h"

BentleyOttmann::BentleyOttmann()
{
	avl = new AVL();
	debug = false;
}

BentleyOttmann::~BentleyOttmann()
{
	delete avl;
}

void BentleyOttmann::addToEventQueue(vector<BOLine*> lineVector)
{
	for (vector<BOLine*>::iterator it = lineVector.begin(); it != lineVector.end(); it++)
	{
		BOLine* line = *it;
		IntersectionPoint ip;
		ip.x = line->start.x;
		ip.y = line->start.y;
		eventQueue.push(Event(ip, line, line, BOEventType::START));
	}

	for (vector<BOLine*>::iterator it = lineVector.begin(); it != lineVector.end(); it++)
	{
		BOLine* line = *it;
		IntersectionPoint ip;
		ip.x = line->end.x;
		ip.y = line->end.y;
		eventQueue.push(Event(ip, line, line, BOEventType::END));
	}
}

void BentleyOttmann::run()
{
	while (!eventQueue.empty())
	{
		Event e = eventQueue.top();
		eventQueue.pop();

		BOLine::sweepX = e.point.x;

		if (debug) cout << "sweepX = " << BOLine::sweepX << endl;
		if (debug) cout << "AVL\n" << *avl << endl;

		IntersectionPoint intersectionPoint;
		if (e.eventType == BOEventType::START)
		{
			if (debug) cout << "START: " << *e.line << endl;
			if (e.line->isVertical)
			{
				if (debug) cout << "VERTICAL" << endl;
				for (set<BOLine*>::iterator it = linesSet.begin(); it != linesSet.end(); it++)
				{
					BOLine* currLine = *it;
					if (e.line->Intersect(*currLine, intersectionPoint) == INTERESECTING)
						intersectionVector.push_back(intersectionPoint);
				}
				continue;

			}

			NodeAvl* insertedNode = avl->Insert(e.line);
			e.line->node = insertedNode;

			NodeAvl* prevNode = avl->pred(insertedNode);
			NodeAvl* nextNode = avl->succ(insertedNode);

			linesSet.insert(e.line);


			if (prevNode)
			{
				BOLine* prevLine = prevNode->pValue;

				if (e.line->Intersect(*prevLine, intersectionPoint) == INTERESECTING)
				{
					if (intersectionPoint.x > BOLine::sweepX)
					{
						eventQueue.push(Event(intersectionPoint, e.line, prevLine, BOEventType::INTERSECTION));
						intersectionVector.push_back(intersectionPoint);
					}
				}
			}

			if (nextNode)
			{
				BOLine* nextLine = nextNode->pValue;

				if (e.line->Intersect(*nextLine, intersectionPoint) == INTERESECTING)
				{
					if (intersectionPoint.x > BOLine::sweepX)
					{
						eventQueue.push(Event(intersectionPoint, e.line, nextLine, BOEventType::INTERSECTION));
						intersectionVector.push_back(intersectionPoint);
					}
				}
			}

		}
		if (e.eventType == BOEventType::END)
		{
			if (debug) cout << "END: " << *e.line << endl;

			if (e.line->isVertical)
			{
				continue;
			}
			NodeAvl* nodeToRemove = e.line->node;

			NodeAvl* prevNode = avl->pred(nodeToRemove);
			NodeAvl* nextNode = avl->succ(nodeToRemove);

			if (prevNode && nextNode)
			{
				BOLine* prevLine = prevNode->pValue;
				BOLine* nextLine = nextNode->pValue;

				if (prevLine->Intersect(*nextLine, intersectionPoint) == INTERESECTING)
				{
					if (intersectionPoint.x > BOLine::sweepX)
					{
						eventQueue.push(Event(intersectionPoint, prevLine, nextLine, BOEventType::INTERSECTION));
						intersectionVector.push_back(intersectionPoint);
					}
				}
			}

			avl->Remove(e.line);
			linesSet.insert(e.line);
		}
		if (e.eventType == BOEventType::INTERSECTION)
		{
			if (debug) cout << "INTERSECTION:\n " << *e.line << "\n " << *e.line2 << endl;

			NodeAvl* node1 = e.line->node;
			NodeAvl* node2 = e.line2->node; //<------- tu bledne node za 4-tym razem

			node1->pValue = e.line2; // u g�ry
			node2->pValue = e.line; // u do�u

			e.line2->node = node1;
			e.line->node = node2;

			NodeAvl* prevNode = avl->pred(node2);
			NodeAvl* nextNode = avl->succ(node1);


			if (prevNode)
			{
				BOLine* prevLine = prevNode->pValue;

				if (e.line->Intersect(*prevLine, intersectionPoint) == INTERESECTING)
				{
					if (intersectionPoint.x > BOLine::sweepX)
					{
						eventQueue.push(Event(intersectionPoint, e.line, prevLine, BOEventType::INTERSECTION));
						intersectionVector.push_back(intersectionPoint);
					}
				}
			}


			if (nextNode)
			{
				BOLine* nextLine = nextNode->pValue;

				if (e.line2->Intersect(*nextLine, intersectionPoint) == INTERESECTING)
				{
					if (intersectionPoint.x > BOLine::sweepX)
					{
						eventQueue.push(Event(intersectionPoint, e.line2, nextLine, BOEventType::INTERSECTION));
						intersectionVector.push_back(intersectionPoint);
					}

				}
			}
		}

		//std::cout << *avl << endl;
	}

	cout << "Punkty przeciecia" << endl;
	for (vector<IntersectionPoint>::iterator it = intersectionVector.begin(); it != intersectionVector.end(); it++)
	{
		cout << *it << endl;
	}
}
