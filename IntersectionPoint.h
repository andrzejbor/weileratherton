#pragma once
#include <cmath>
#include <ostream>
#include "BOGlobal.h"

using std::ostream;

struct IntersectionPoint
{
	double x;
	double y;
	int index[2];

	IntersectionPoint()
	{
		IntersectionPoint(0,0);
	}

	IntersectionPoint(double x, double y)
	{
		this->x = x;
		this->y = y;
		index[0] = 0;
		index[1] = 0;
	}

	IntersectionPoint operator+ (IntersectionPoint &p)
	{
		return IntersectionPoint (x + p.x, y + p.y);
	}

	IntersectionPoint operator- (IntersectionPoint &p)
	{
		return IntersectionPoint (x + p.x, y + p.y);
	}

	IntersectionPoint operator* (double d)
	{
		return IntersectionPoint (d * x, d * y);
	}

	int operator==(const IntersectionPoint &p)
	{
		return EQ(x, p.x) && EQ(y, p.y);
	}

	int operator!=(const IntersectionPoint &p)
	{
		return !EQ(x, p.x) || !EQ(y, p.y);
	}

	friend ostream & operator<< (ostream &out, const IntersectionPoint &p)
	{
		return out << "[" << p.x << "," << p.y  << " " << p.index[0] << "," << p.index[1] << "]";
	}
};
